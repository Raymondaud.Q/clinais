# This is an integration

I (Quentin.R) have just integrated https://codesandbox.io/s/jmo19?file=/App.svelte in our projet 

# Original README 

A recreation of [this iOS UI demo](https://github.com/Cuberto/rate-it) by [Cuberto](https://cuberto.com).

It's built with [Svelte](https://svelte.dev) and uses [d3-interpolate](https://github.com/d3/d3-interpolate) for svg path morphing.

Made by [me](https://dribbble.com/cwgw) as an exercise in learning the Svelte API.

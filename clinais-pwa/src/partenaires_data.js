import { writable } from 'svelte/store';

const partenaires = writable([
	{
		nom: 'Bienvenue, toute l\'équipe de la clinique du Vernais vous souhaite une merveilleuse année 2022! ',
		url: 'https://gitlab.com/Raymondaud.Q/clinais/raw/main/external_resources/Logo.png',
		details:[
		{
			description :'La clinique vétérinaire du Vernais regroupe 10 vétérinaires et nous disposons de deux blocs opératoires. Nous sommes à votre disposition pour tous les conseils et informations nécessaires à la santé et au bien être de votre animal. Lors de vos visites, n\'hésitez pas à demander conseils, prendre rendez-vous ou de consulter les services de nos partenaires',
			activity : 'Hospitalisation, chirurgie, diagnostic sur place et diagnostic à distance ',
			link:'https://www.clinais.fr'
		}],				
	},
	{
		nom: 'AniVoyage',
		url: 'https://64.media.tumblr.com/aea99ac5e307dba3375f0f5cc700789c/tumblr_omaaaxEuAX1v8d5b1o1_640.jpg',
		details:[
		{
			description :'Préparer le voyage de votre animal de compagnie avec l\'expert en éducation canine. Le site AniVoyage vous propose en plus de l\'éducation canine, une pension et le toilettage de votre chien.',
			activity : 'Dressage chien - Pension',
			link:'https://www.aniVoyage.fr'
		}],
	},
	{
		nom: 'Squeezie',
		url: 'https://i.pinimg.com/564x/90/ce/58/90ce584fcf97e5f9dc4793ea5370380a.jpg',
		details:[
		{
			description :'Découvrez les harnais connectés pour votre chien ! Retrouvez toutes les vidéos sur la chaîne Youtube de Squeezie.',
			activity : 'Sport, Analyse de performance des chiens',
			link:'https://www.youtube.com/watch?v=hNS43l4qXvg&t=3s&ab_channel=SQUEEZIE'
		}],
	},
	{
		nom: 'Concours photos',
		url: 'images/photo.png',
		details:[
		{
			description :'Nous organisons un concours photos, le thèmes de ce concours sera : La nouvelle année! Tentez votre chance pour gagner un mois de croquettes vétérinaires pour votre animal. Envoyez nous vos photos à l\'adreese dans la rubrique contact avant le 3 janvier 2022',
			activity : 'Concours',
			link:'contact@clinique-veterinaire-du-vernais.fr'
		}],
	},
]);

export default partenaires;

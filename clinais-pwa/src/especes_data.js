import { writable } from 'svelte/store';

const especes = writable([
	{
		nom: 'Chat',
		url : 'images/chien.jpg'
	},
	{
		nom: 'Chien',
		url: 'images/chat.png'
	},
	{
		nom: 'Poisson',
		url : 'images/poisson.png'
	},
	{
		nom: 'Rongeur',
		url : 'images/souris.jpg'
	},
	{
		nom: 'Reptile',
		url : 'images/reptile.jpg'
	},
	{
		nom: 'Oiseau',
		url : 'images/oiseaux.jpg'
	},
	
])

export default especes;
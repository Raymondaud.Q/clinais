This project is made with https://kit.svelte.dev/docs

# Install Clinais-PWA

* cd clinais-pwa
* npm i

# Run dev server

* cd clinais-pwa
* npm run dev -- --host
